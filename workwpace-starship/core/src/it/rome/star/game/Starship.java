package it.rome.star.game;

import it.rome.star.game.screen.MainMenuScreen;

import com.badlogic.gdx.Game;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class Starship extends Game {

  @Override
  public void create() {
    setScreen(new MainMenuScreen(this));
  }

}
