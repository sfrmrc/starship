package it.rome.star.game.screen;

import it.rome.game.screen.AbstractBaseScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public class GameOverScreen extends AbstractBaseScreen {

  private Texture fontTexture = new Texture(Gdx.files.internal("fonts/normal_0.png"));

  private Stage stage;
  private Label backButton;

  public GameOverScreen(final Game game, int scoreValue) {
    super(game);
    stage = new Stage();
    float halfWidth = stage.getWidth() * .5f;
    float halfHeight = stage.getHeight() * .5f;
    TextureRegion fontRegion = new TextureRegion(fontTexture);
    BitmapFont bitmapFont =
        new BitmapFont(Gdx.files.internal("fonts/normal.fnt"), fontRegion, false);
    LabelStyle oldStyle = new LabelStyle(bitmapFont, Color.WHITE);
    backButton = new Label("Back", oldStyle);
    backButton.setTouchable(Touchable.enabled);
    backButton.setBounds(0, 0, backButton.getWidth(), backButton.getHeight());
    backButton.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        game.setScreen(new MainMenuScreen(game));
        return true;
      }
    });
    Label gameOver = new Label("Game Over", oldStyle);
    gameOver.setPosition(halfWidth - 40, halfHeight + gameOver.getHeight());
    gameOver.setColor(Color.RED);
    Label score = new Label("Score: " + scoreValue, oldStyle);
    score.setPosition(halfWidth - 40, halfHeight - gameOver.getHeight());
    stage.addActor(backButton);
    stage.addActor(gameOver);
    stage.addActor(score);
    Gdx.input.setInputProcessor(stage);
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    stage.act(delta);
    stage.draw();
  }

  @Override
  public void hide() {
    stage.dispose();
  }

}
