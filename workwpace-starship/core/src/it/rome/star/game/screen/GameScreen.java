package it.rome.star.game.screen;

import it.rome.game.component.ScoreComponent;
import it.rome.game.screen.AbstractBaseScreen;
import it.rome.star.game.stage.PlayStage;
import it.rome.star.game.template.Tags;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Shows game
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class GameScreen extends AbstractBaseScreen {

  com.badlogic.gdx.physics.box2d.World physicWorld;
  OrthographicCamera camera;

  Stage view;
  TagManager tagManager;
  GroupManager groupManager;

  public GameScreen(Game game) {
    super(game);
    float w = Gdx.graphics.getWidth();
    float h = Gdx.graphics.getHeight();
    camera = new OrthographicCamera(w, h);
    camera.setToOrtho(false, w, h);
    camera.update();
    initWorld();
    view = new PlayStage(world, physicWorld, camera);
  }

  private void initWorld() {
    this.world = new World();
    this.physicWorld = new com.badlogic.gdx.physics.box2d.World(new Vector2(-10, 0), true);
    tagManager = new TagManager();
    groupManager = new GroupManager();
    world.setManager(tagManager);
    world.setManager(groupManager);
    world.initialize();
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    world.setDelta(delta);
    world.process();
    view.act(delta);
    view.draw();
    if (tagManager.getEntity(Tags.Starship) == null) {
      Entity score = tagManager.getEntity(Tags.Score);
      ScoreComponent scoreComponent = score.getComponent(ScoreComponent.class);
      game.setScreen(new GameOverScreen(game, scoreComponent.getScore()));
    }
  }

  @Override
  public void hide() {
    super.dispose();
    view.dispose();
    world.dispose();
  }

}
