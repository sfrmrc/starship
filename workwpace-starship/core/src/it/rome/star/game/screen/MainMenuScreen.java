package it.rome.star.game.screen;

import it.rome.game.screen.AbstractBaseScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * Shows main menu
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class MainMenuScreen extends AbstractBaseScreen {

  private Texture fontTexture = new Texture(Gdx.files.internal("fonts/normal_0.png"));

  private Stage stage;
  private Label startButton;
  private Label aboutButton;

  public MainMenuScreen(final Game game) {
    super(game);
    stage = new Stage();
    TextureRegion fontRegion = new TextureRegion(fontTexture);
    BitmapFont bitmapFont =
        new BitmapFont(Gdx.files.internal("fonts/normal.fnt"), fontRegion, false);

    LabelStyle style = new LabelStyle(bitmapFont, Color.WHITE);
    startButton = new Label("Start", style);
    startButton.setTouchable(Touchable.enabled);
    startButton.setBounds(stage.getWidth() * .5f - 30, stage.getHeight() * .5f + 25,
        startButton.getWidth(), startButton.getHeight());
    startButton.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        game.setScreen(new GameScreen(game));
        return true;
      }
    });
    aboutButton = new Label("About", style);
    aboutButton.setTouchable(Touchable.enabled);
    aboutButton.setBounds(stage.getWidth() * .5f - 30, stage.getHeight() * .5f - 25,
        aboutButton.getWidth(), aboutButton.getHeight());
    aboutButton.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        game.setScreen(new AboutScreen(game));
        return true;
      }
    });
    stage.addActor(startButton);
    stage.addActor(aboutButton);
    Gdx.input.setInputProcessor(stage);
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    stage.act(delta);
    stage.draw();
  }

  @Override
  public void hide() {
    stage.dispose();
  }

}
