package it.rome.star.game.stage;

import it.rome.game.attribute.Bound;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.ScoreComponent;
import it.rome.game.screen.AbstractBaseStage;
import it.rome.game.system.ExpiringSystem;
import it.rome.game.system.PhysicsSystem;
import it.rome.game.system.PhysicsUpdateSystem;
import it.rome.game.system.RenderableSystem;
import it.rome.game.system.ScoreRenderSystem;
import it.rome.game.system.ScriptSystem;
import it.rome.game.system.SpriteAnimationSystem;
import it.rome.game.system.SpritePositionSystem;
import it.rome.game.system.WASDInputSystem;
import it.rome.game.system.render.Layer;
import it.rome.game.system.render.RenderableLayer;
import it.rome.game.template.EntityFactory;
import it.rome.game.template.Parameters;
import it.rome.star.game.template.AimTemplate;
import it.rome.star.game.template.AsteroidTemplate;
import it.rome.star.game.template.LevelTemplate;
import it.rome.star.game.template.StarTemplate;
import it.rome.star.game.template.StarshipTemplate;
import it.rome.star.game.template.Tags;

import java.util.Random;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Play
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class PlayStage extends AbstractBaseStage {

  private com.badlogic.gdx.physics.box2d.World physicWorld;
  private OrthographicCamera camera;
  private RenderableLayer renderableLayer;

  private EntityFactory entityFactory;
  private BodyBuilder bodyBuilder;

  public PlayStage(World world, com.badlogic.gdx.physics.box2d.World physicWorld,
      OrthographicCamera camera) {
    super(world);
    this.camera = camera;
    this.physicWorld = physicWorld;
    bodyBuilder = new BodyBuilder(physicWorld);
    entityFactory = new EntityFactory(world);
    SpriteBatch spriteBatch = new SpriteBatch();
    Layer background = new Layer(1, camera, spriteBatch);
    Layer asteroid = new Layer(2, camera, spriteBatch);
    Layer starship = new Layer(3, camera, spriteBatch);
    renderableLayer = new RenderableLayer();
    renderableLayer.add("star", background);
    renderableLayer.add("asteroid", asteroid);
    renderableLayer.add("starship", starship);
    initWorld();
    initGame();
  }

  private void initWorld() {
    world.setSystem(new ExpiringSystem());
    // world.setSystem(new HealtIndicatorSystem());
    world.setSystem(new ScriptSystem());
    world.setSystem(new WASDInputSystem());
    world.setSystem(new PhysicsSystem(physicWorld));
    world.setSystem(new PhysicsUpdateSystem());
    world.setSystem(new SpritePositionSystem());
    world.setSystem(new SpriteAnimationSystem());
    world.setSystem(new ScoreRenderSystem());
    // world.setSystem(new EntityDebugSystem());
    // world.setSystem(new Box2dDebugSystem(camera, physicWorld));
    // world.setSystem(new FpsDebugSystem());
    world.setSystem(new RenderableSystem(camera, renderableLayer));
  }

  private void initGame() {
    generateStars();
    generateStarship();
    generateAim();
    generateLevel();
  }

  private void generateStars() {
    Random rnd = new Random();
    StarTemplate starTemplate = new StarTemplate();
    for (int i = 0; i < 50; i++) {
      Float y = new Float(rnd.nextInt(getIntHeight()));
      Float x = new Float(rnd.nextInt(getIntWidth()));
      Float life = new Float(rnd.nextInt(50));
      entityFactory.instantiate(starTemplate,
          new Parameters().put("x", x).put("y", y).put("life", life));
    }
  }

  private void generateStarship() {
    Entity starship =
        entityFactory.instantiate(
            new StarshipTemplate(bodyBuilder),
            new Parameters().put("spatial", new SpatialImpl(40, 240, 60, 30)).put("bound",
                new Bound(0, 0, camera.viewportWidth, camera.viewportHeight)));
    Entity score = world.createEntity();
    score.edit().add(starship.getComponent(ScoreComponent.class));
    TagManager tagManager = world.getManager(TagManager.class);
    tagManager.register(Tags.Starship, starship);
    tagManager.register(Tags.Score, score);
  }

  private void generateAim() {
    @SuppressWarnings("unused")
    Entity aim =
        entityFactory.instantiate(new AimTemplate(entityFactory, bodyBuilder),
            new Parameters().put("caster", Tags.Starship));
  }

  private void generateLevel() {
    LevelTemplate.Level info = new LevelTemplate.Level() {

      Random rnd = new Random();

      private int stack = 0;
      private float step = 1f;
      private float speed = -50f;
      private float tick = 0, every = 3;
      private float life = 10000;

      private final AsteroidTemplate asteroidTemplate = new AsteroidTemplate(bodyBuilder);

      @Override
      public float getLife() {
        return life;
      }

      @Override
      public void tick(float delta) {
        tick += delta;
        if (tick > every) {
          if (stack++ % 10 == 0) {
            step += .5f;
          }
          if (every > 0.3f) {
            every -= 0.1f;
          }
          tick = 0;
          float h = rnd.nextInt(getIntHeight());
          int type = rnd.nextInt(2);
          SpatialImpl spatial = new SpatialImpl(getIntWidth(), h, 40f, 40f);
          entityFactory.instantiate(
              asteroidTemplate,
              new Parameters().put("spatial", spatial).put("type", type)
                  .put("movement", new Vector2(speed * step, 0)));
        }
      }
    };
    LevelTemplate levelTemplate = new LevelTemplate();
    entityFactory.instantiate(levelTemplate, new Parameters().put("level", info));
  }

  public int getIntHeight() {
    return new Float(super.getHeight()).intValue();
  }

  public int getIntWidth() {
    return new Float(super.getWidth()).intValue();
  }

}
