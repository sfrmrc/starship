package it.rome.star.game.template;

import it.rome.game.attribute.impl.ScriptJava;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.ScriptComponent;
import it.rome.game.template.EntityFactory;
import it.rome.game.template.EntityTemplate;
import it.rome.game.template.Parameters;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.World;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

/**
 * Creates ...
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class AimTemplate extends EntityTemplate {

  private EntityFactory entityFactory;
  private BodyBuilder bodyBuilder;

  public AimTemplate(EntityFactory entityFactory, BodyBuilder bodyBuilder) {
    this.entityFactory = entityFactory;
    this.bodyBuilder = bodyBuilder;
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();

    String caster = parameters.get("caster");
    if (caster == null || "".equals(caster)) {
      throw new IllegalArgumentException("Caster parameter cannot be null or empty string");
    }

    TagManager manager = entity.getWorld().getManager(TagManager.class);
    WeaponScript script =
        new WeaponScript(entityFactory, bodyBuilder, manager, Tags.Starship, 0.5f);
    entityEdit.add(new ScriptComponent(script));

  }

  private class WeaponScript extends ScriptJava {

    private EntityFactory entityFactory;
    private BulletTemplate bulletTemplate;
    private String caster;
    private float countdown;
    private float tmp = 0;

    public WeaponScript(EntityFactory entityFactory, BodyBuilder bodyBuilder,
        TagManager tagManager, String caster, float countdown) {
      this.entityFactory = entityFactory;
      this.caster = caster;
      this.countdown = countdown;
      this.bulletTemplate = new BulletTemplate(tagManager, bodyBuilder);
    }

    @Override
    public void execute(World world, Entity entity) {
      if (tmp > 0) {
        tmp -= world.getDelta();
      }
      if (tmp <= 0) {
        tmp = 0;
      }
      if (Gdx.input.isKeyPressed(Keys.SPACE) && tmp == 0) {
        tmp = countdown;
        entityFactory.instantiate(bulletTemplate, new Parameters().put("caster", caster));
      }
    }
  }

}
