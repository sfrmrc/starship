package it.rome.star.game.template;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.ScriptJava;
import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.ExpiresComponent;
import it.rome.game.component.FadingComponent;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.ScriptComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteAnimationComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.World;
import com.artemis.fsm.EntityStateMachine;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * Creates the asteroid
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class AsteroidTemplate extends EntityTemplate {

  public enum State {
    ALIVE, DEAD
  }

  private final BodyBuilder bodyBuilder;
  private final Texture exposion;
  private final Texture[] asteroid = new Texture[3];

  public AsteroidTemplate(BodyBuilder bodyBuilder) {
    this.bodyBuilder = bodyBuilder;
    this.exposion = new Texture("explosions.png");
    this.asteroid[0] = new Texture("asteroid1.png");
    this.asteroid[1] = new Texture("asteroid2.png");
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spatial spatial = parameters.get("spatial");
    Vector2 movement = parameters.get("movement");
    Integer type = parameters.get("type");

    if (spatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    if (movement != null) {
      MovementComponent movementComponent = new MovementComponent(movement);
      entityEdit.add(movementComponent);
    }

    if (type == null) {
      throw new IllegalArgumentException("Type parameter cannot be null [1, 2]");
    }

    Body body =
        bodyBuilder
            .userData(entity)
            .type(BodyType.DynamicBody)
            .position(spatial.getX(), spatial.getY())
            .fixture(
                bodyBuilder.fixtureDefBuilder()
                    .boxShape(spatial.getWidth() * 0.3f, spatial.getHeight() * 0.3f)
                    .restitution(0f).density(1f).friction(1f)).build();

    PhysicsComponent physicsComponent = new PhysicsComponent(body);
    SpatialComponent spatialComponent = new SpatialComponent(new SpatialPhysicsImpl(body, spatial));

    final EntityStateMachine<State> esm = new EntityStateMachine<AsteroidTemplate.State>(entity);

    ScriptComponent explodeScript = new ScriptComponent(new ScriptJava() {

      @Override
      public void execute(World world, Entity entity) {
        PhysicsComponent physicsComponent = entity.getComponent(PhysicsComponent.class);
        if (physicsComponent.getPhysics().getContacts().isEmpty()) {
          return;
        }
        esm.changeState(State.DEAD);
      }
    });

    entityEdit.add(new RenderableComponent(2));
    entityEdit.add(spatialComponent);
    entityEdit.add(physicsComponent);
    entityEdit.add(explodeScript);
    entityEdit.add(new ExpiresComponent(30f));

    createAliveState(esm, entity, type);
    createDeadState(esm, entity);

    esm.changeState(State.ALIVE);

  }

  private void createAliveState(EntityStateMachine<State> esm, Entity entity, int type) {
    Sprite sprite = new Sprite(asteroid[type]);
    SpriteAnimationComponent spriteAnimationComponent =
        new SpriteAnimationComponent(asteroid[type], 0f, 0.1f, PlayMode.LOOP, 5, 4, 19);
    SpriteComponent spriteComponent = new SpriteComponent(sprite);
    esm.createState(State.ALIVE).add(SpriteAnimationComponent.class)
        .withInstance(spriteAnimationComponent).add(SpriteComponent.class)
        .withInstance(spriteComponent);
  }

  private void createDeadState(EntityStateMachine<State> esm, Entity entity) {
    Sprite sprite = new Sprite(exposion);
    SpriteAnimationComponent spriteAnimationComponent =
        new SpriteAnimationComponent(exposion, 0f, 0.06f, PlayMode.NORMAL, 8, 6);
    SpriteComponent spriteComponent = new SpriteComponent(sprite);
    FadingComponent fadingComponent = new FadingComponent(1f);
    ExpiresComponent expiresComponent = new ExpiresComponent(1f);
    esm.createState(State.DEAD).add(SpriteAnimationComponent.class)
        .withInstance(spriteAnimationComponent).add(SpriteComponent.class)
        .withInstance(spriteComponent).add(FadingComponent.class).withInstance(fadingComponent)
        .add(ExpiresComponent.class).withInstance(expiresComponent);
  }

}
