package it.rome.star.game.template;

import it.rome.game.attribute.Script;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.ScriptJava;
import it.rome.game.component.CasterComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.ScoreComponent;

import java.util.Collection;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.physics.box2d.Contact;

/**
 * Creates {@link Script} for bullet, when this {@link Entity} collide with an other will be removed
 * from {@link World}
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
class BulletScriptJava extends ScriptJava {

  private Spatial spatial;
  private float len = 0;

  public BulletScriptJava(Spatial spatial) {
    this.spatial = spatial;
  }

  @Override
  public void execute(World world, Entity entity) {
    checkLife(entity, world.delta);
    checkCollision(world, entity);
  }

  private void checkCollision(World world, Entity entity) {
    PhysicsComponent physicsComponent = entity.getComponent(PhysicsComponent.class);
    Collection<Contact> contacts = physicsComponent.getPhysics().getContacts();
    if (contacts.isEmpty()) return;
    CasterComponent casterComponent = entity.getComponent(CasterComponent.class);
    if (casterComponent.getEntityRef().getEntity() != null) {
      Entity caster = casterComponent.getEntityRef().getEntity();
      caster.getComponent(ScoreComponent.class).increment(1);
    }
    entity.deleteFromWorld();
  }

  private void checkLife(Entity entity, float delta) {
    len += 200 * delta;
    spatial.setPosition(spatial.getX(), spatial.getY() + len);
    if (len > 500f) {
      this.finished = true;
    }
  }

}
