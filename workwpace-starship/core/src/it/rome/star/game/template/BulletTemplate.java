package it.rome.star.game.template;

import it.rome.game.attribute.EntityRef;
import it.rome.game.attribute.Script;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.CasterComponent;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.ScriptComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * Creates the bullet
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class BulletTemplate extends EntityTemplate {

  private BodyBuilder bodyBuilder;
  private TagManager tagManager;

  public BulletTemplate(TagManager tagManager, BodyBuilder bodyBuilder) {
    this.tagManager = tagManager;
    this.bodyBuilder = bodyBuilder;
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    String caster = parameters.get("caster");

    if (caster == null || "".equals(caster)) {
      throw new IllegalArgumentException("Caster parameter cannot be null");
    }

    Entity entityCaster = tagManager.getEntity(caster);

    Spatial spatialCaster = entityCaster.getComponent(SpatialComponent.class).getSpatial();
    SpatialImpl spatial =
        new SpatialImpl(spatialCaster.getX() + spatialCaster.getWidth(), spatialCaster.getY(), 10,
            20);

    Script script = new BulletScriptJava(spatial);
    Body body =
        bodyBuilder.userData(entity).type(BodyType.KinematicBody)
            .position(spatial.getX(), spatial.getY())
            .fixture(bodyBuilder.fixtureDefBuilder().circleShape(10f).density(1f)).build();

    Spatial spatialPhysics = new SpatialPhysicsImpl(body, spatial);

    PhysicsComponent physicsComponent = new PhysicsComponent(body);
    ScriptComponent scriptComponent = new ScriptComponent(script);
    MovementComponent movementComponent = new MovementComponent(new Vector2(100, 0));
    SpriteComponent spriteComponent = new SpriteComponent(new Sprite(new Texture("redLaserRay.png")));
    SpatialComponent spatialComponent = new SpatialComponent(spatialPhysics);

    entityEdit.add(new CasterComponent(new EntityRef(entityCaster)));
    entityEdit.add(spriteComponent);
    entityEdit.add(movementComponent);
    entityEdit.add(physicsComponent);
    entityEdit.add(scriptComponent);
    entityEdit.add(spatialComponent);
    entityEdit.add(new RenderableComponent(1));
  }

}
