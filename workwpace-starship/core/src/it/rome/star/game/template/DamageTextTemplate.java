package it.rome.star.game.template;

import it.rome.game.component.DamageComponent;
import it.rome.game.component.FadingComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.TextComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Creates text for damage information
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class DamageTextTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    SpatialComponent spatialComponent = parameters.get("spatial");
    if (spatialComponent == null)
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    DamageComponent damageComponent = parameters.get("damage");
    if (damageComponent == null)
      throw new IllegalArgumentException("Damage parameter cannot be null");
    BitmapFont font = parameters.get("font");
    if (font == null) {
      font = new BitmapFont();
    }
    String damage = "-" + damageComponent.getDamage();
    TextComponent textComponent = new TextComponent(damage, font, Color.RED);
    FadingComponent fadingComponent = new FadingComponent(3);
    entityEdit.add(textComponent);
    entityEdit.add(fadingComponent);
  }

}
