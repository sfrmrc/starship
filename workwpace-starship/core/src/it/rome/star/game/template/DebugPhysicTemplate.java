package it.rome.star.game.template;

import it.rome.game.attribute.impl.ScriptJava;
import it.rome.game.component.ScriptComponent;
import it.rome.game.component.TextComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Component;
import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.World;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class DebugPhysicTemplate extends EntityTemplate {

  private static final BitmapFont font = new BitmapFont();

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    TextComponent textComponent = new TextComponent("", font, Color.WHITE);
    DebugPhysicCountComponent debugPhysicCountComponent = new DebugPhysicCountComponent();
    ScriptComponent scriptComponent = new ScriptComponent(new CollisionCountScript());
    entityEdit.add(textComponent);
    entityEdit.add(scriptComponent);
    entityEdit.add(debugPhysicCountComponent);
  }

  private static class CollisionCountScript extends ScriptJava {

    @Override
    public void execute(World world, Entity entity) {
      TextComponent textComponent = entity.getComponent(TextComponent.class);
      DebugPhysicCountComponent debugPhysicCountComponent =
          entity.getComponent(DebugPhysicCountComponent.class);
      textComponent.setText(debugPhysicCountComponent.count.toString());
    }
  }

  private class DebugPhysicCountComponent extends Component {
    public Integer count = 0;
  }

}
