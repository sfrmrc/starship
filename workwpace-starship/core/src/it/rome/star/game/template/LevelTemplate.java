package it.rome.star.game.template;

import it.rome.game.attribute.impl.ScriptJava;
import it.rome.game.component.ScriptComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.World;

/**
 * Level template adds {@link RespawnScript} that allows to respawn elements into the level
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class LevelTemplate extends EntityTemplate {

  /**
   * Defines the rate of update level
   * 
   * @author m.sferra
   * 
   * @since 1.0.0
   * 
   */
  public interface Level {

    public void tick(float delta);

    public float getLife();

  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Level level = parameters.get("level");

    if (level == null) {
      throw new IllegalArgumentException("Level parameter cannot be null");
    }

    entityEdit.add(new ScriptComponent(new RespawnScript(level)));
  }

  /**
   * Allows {@link Entity} to be added into the world at rate level
   * 
   * @author marco
   * 
   * @since 1.0.0
   * 
   */
  class RespawnScript extends ScriptJava {

    private float life;
    private Level level;

    public RespawnScript(Level level) {
      this.level = level;
    }

    @Override
    public void execute(World world, Entity entity) {
      level.tick(world.getDelta());
      updateLevel(world);
    }

    private void updateLevel(World world) {
      life += world.getDelta();
      if (life >= level.getLife()) {
        finished = true;
      }
    }

  }

}
