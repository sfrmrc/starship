package it.rome.star.game.template;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.ExpiresComponent;
import it.rome.game.component.FadingComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteAnimationComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * <p>
 * Creates a star with {@link SpriteComponent}, {@link SpriteAnimationComponent},
 * {@link PhysicsComponent}, {@link ExpiresComponent}
 * </p>
 * <p>
 * Required parameters:
 * <ul>
 * <li>x float</li>
 * <li>y float</li>
 * <li>life float</li>
 * </ul>
 * </p>
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class StarTemplate extends EntityTemplate {

  private final Texture star = new Texture("star.png");

  public StarTemplate() {}

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();

    Float x = parameters.get("x");
    Float y = parameters.get("y");
    Float life = parameters.get("life");

    if (x == null || y == null) {
      throw new IllegalArgumentException("X and Y parameter cannot be null");
    }

    if (life == null) {
      throw new IllegalArgumentException("Life parameter cannot be null");
    }

    Spatial spatial = new SpatialImpl(x, y, 5, 5);

    SpriteComponent spriteComponent = new SpriteComponent(new Sprite(star));
    SpatialComponent spatialComponent = new SpatialComponent(spatial);
    FadingComponent fadingComponent = new FadingComponent(life, true);

    entityEdit.add(new RenderableComponent(1));
    entityEdit.add(spriteComponent);
    entityEdit.add(spatialComponent);
    entityEdit.add(fadingComponent);
  }

}
