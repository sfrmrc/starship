package it.rome.star.game.template;

import it.rome.game.attribute.Bound;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.ScriptJava;
import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.BoundComponent;
import it.rome.game.component.InputComponent;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.PlayerComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.ScoreComponent;
import it.rome.game.component.ScriptComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteAnimationComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.World;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * Creates a starship with {@link SpriteComponent}, {@link SpriteAnimationComponent},
 * {@link PhysicsComponent}, {@link TagComponent} with value {@link Tags#Starship},
 * {@link MovementComponent}, {@link InputComponent}, {@link VelocityComponent},
 * {@link MovementComponent} {@link ScoreComponent}
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class StarshipTemplate extends EntityTemplate {

  BodyBuilder bodyBuilder;

  public StarshipTemplate(BodyBuilder bodyBuilder) {
    this.bodyBuilder = bodyBuilder;
  }

  @Override
  public void apply(final Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spatial spatial = parameters.get("spatial");
    Bound bound = parameters.get("bound");

    if (spatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    Body body =
        bodyBuilder
            .userData(entity)
            .type(BodyType.KinematicBody)
            .position(spatial.getX(), spatial.getY())
            .fixture(
                bodyBuilder.fixtureDefBuilder()
                    .boxShape(spatial.getWidth() * 0.3f, spatial.getHeight() * 0.3f)
                    .restitution(0f).density(1f).friction(0f).build()).build();

    ScriptJava explodeScript = new ScriptJava() {

      @Override
      public void execute(World world, Entity entity) {
        PhysicsComponent physicsComponent = entity.getComponent(PhysicsComponent.class);
        if (physicsComponent.getPhysics().getContacts().isEmpty()) {
          return;
        }
        entity.deleteFromWorld();
      }
    };

    if (bound != null) {
      entityEdit.add(new BoundComponent(bound));
    }
    
    entityEdit.add(new ScriptComponent(explodeScript));
    entityEdit.add(new ScoreComponent(0));
    entityEdit.add(new TagComponent(Tags.Starship));
    entityEdit.add(new PlayerComponent());
    entityEdit.add(new InputComponent());
    entityEdit.add(new RenderableComponent(3));
    entityEdit.add(new SpriteComponent(new Sprite(new Texture("starship.png"))));
    entityEdit.add(new VelocityComponent(new Vector2(100, 100)));
    entityEdit.add(new MovementComponent());
    entityEdit.add(new PhysicsComponent(body));
    entityEdit.add(new SpatialComponent(new SpatialPhysicsImpl(body, spatial.getWidth(), spatial
        .getHeight())));
  }

}
