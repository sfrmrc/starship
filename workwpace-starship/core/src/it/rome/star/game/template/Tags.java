package it.rome.star.game.template;

/**
 * Tags
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class Tags {

  public static final String Starship = "Starship";
  public static final String Score = "Score";
  public static final String Wall = "Wall";
  public static final String Camera = "Camera";
  public static final String Pov = "Pov";

}
