

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class RAWTiledMapTest extends ApplicationAdapter implements InputProcessor {

  public static void main(String[] args) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    new LwjglApplication(new RAWTiledMapTest(), config);
  }

  OrthographicCamera camera;
  FPSLogger logger;
  TiledMap tiledMap;
  TiledMapRenderer tiledMapRenderer;

  @Override
  public void create() {
    logger = new FPSLogger();
    float w = Gdx.graphics.getWidth();
    float h = Gdx.graphics.getHeight();
    camera = new OrthographicCamera(w, h);
    camera.setToOrtho(false, w, h);
    camera.update();
    tiledMap = new TmxMapLoader().load("desert.tmx");
    for (MapLayer mapLayer : tiledMap.getLayers()) {
      if ("obstacles".equals(mapLayer.getName())) {
        for (MapObject mapObject : mapLayer.getObjects()) {
          Object x = mapObject.getProperties().get("x");
          Gdx.app.log("mapObject", "Reading value x " + x);
          Object y = mapObject.getProperties().get("y");
          Gdx.app.log("mapObject", "Reading value y " + y);
        }
      }
    }
    tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
    Gdx.input.setInputProcessor(this);
  }

  @Override
  public void render() {
    Gdx.gl.glClearColor(1, 0, 0, 1);
    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    camera.update();
    logger.log();
    tiledMapRenderer.setView(camera);
    tiledMapRenderer.render();
  }

  @Override
  public boolean keyDown(int keycode) {
    return false;
  }

  @Override
  public boolean keyUp(int keycode) {
    if (keycode == Input.Keys.LEFT) camera.translate(-32, 0);
    if (keycode == Input.Keys.RIGHT) camera.translate(32, 0);
    if (keycode == Input.Keys.UP) camera.translate(0, -32);
    if (keycode == Input.Keys.DOWN) camera.translate(0, 32);
    if (keycode == Input.Keys.NUM_1)
      tiledMap.getLayers().get(0).setVisible(!tiledMap.getLayers().get(0).isVisible());
    if (keycode == Input.Keys.NUM_2)
      tiledMap.getLayers().get(1).setVisible(!tiledMap.getLayers().get(1).isVisible());
    return false;
  }

  @Override
  public boolean keyTyped(char character) {

    return false;
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    return false;
  }

  @Override
  public boolean touchUp(int screenX, int screenY, int pointer, int button) {
    return false;
  }

  @Override
  public boolean touchDragged(int screenX, int screenY, int pointer) {
    return false;
  }

  @Override
  public boolean mouseMoved(int screenX, int screenY) {
    return false;
  }

  @Override
  public boolean scrolled(int amount) {
    return false;
  }

}
