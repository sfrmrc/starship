package it.rome.game;

import org.junit.Before;
import org.mockito.Mockito;

import com.artemis.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.mock.graphics.MockGraphics;
import com.badlogic.gdx.backends.lwjgl.LwjglFiles;
import com.badlogic.gdx.backends.lwjgl.LwjglNativesLoader;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;

public abstract class BaseWorldTest {

  protected World world;
  protected com.badlogic.gdx.physics.box2d.World physicWorld;

  @Before
  public void setup() {
    LwjglNativesLoader.load();
    Gdx.files = new LwjglFiles();
    Gdx.graphics = new MockGraphics();
    Gdx.gl = Mockito.mock(GL20.class);
    physicWorld = new com.badlogic.gdx.physics.box2d.World(new Vector2(), false);
    world = new World();
    initSystem();
    world.initialize();
    initEntities();
  }

  protected abstract void initSystem();

  protected abstract void initEntities();

}
