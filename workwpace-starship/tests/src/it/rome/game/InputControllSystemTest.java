package it.rome.game;

import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.InputComponent;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.system.PhysicsSystem;
import it.rome.game.system.RenderableSystem;
import it.rome.game.system.SpritePositionSystem;
import it.rome.game.system.WASDInputSystem;
import it.rome.game.template.Parameters;
import it.rome.game.template.mock.BoxTemplate;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Creates a box shape controlled by WASD system
 * 
 * @author m.sferra
 * 
 */
public class InputControllSystemTest extends BaseGameTest {

  public static void main(String[] args) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    new LwjglApplication(new InputControllSystemTest(), config);
  }

  private World physicWorld;

  @Override
  protected void createEntites() {
    physicWorld = new World(new Vector2(), false);
    BodyBuilder bodyBuilder = new BodyBuilder(physicWorld);
    entityFactory
        .instantiate(new BoxTemplate(bodyBuilder),
            new Parameters().put("spatial", new SpatialImpl(320, 40, 40, 40)))
        .addComponent(new VelocityComponent(new Vector2(20, 20)))
        .addComponent(new MovementComponent()).addComponent(new InputComponent());
  }

  @Override
  protected void createLayers() {}

  @Override
  protected void addSystems() {
    world.setSystem(new RenderableSystem(camera, renderableLayer));
    world.setSystem(new WASDInputSystem());
    world.setSystem(new PhysicsSystem(physicWorld));
    world.setSystem(new SpritePositionSystem());
  }

}
