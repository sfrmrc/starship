package it.rome.game;

import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.FadingComponent;
import it.rome.game.system.Box2dDebugSystem;
import it.rome.game.system.EntityDebugSystem;
import it.rome.game.system.FpsDebugSystem;
import it.rome.game.system.PhysicsSystem;
import it.rome.game.system.RenderableSystem;
import it.rome.game.system.SpriteAnimationSystem;
import it.rome.game.system.SpritePositionSystem;
import it.rome.game.template.FloorTemplate;
import it.rome.game.template.Parameters;
import it.rome.game.template.mock.BoxTemplate;
import it.rome.game.template.mock.RunnerTemplate;
import it.rome.star.game.template.AsteroidTemplate;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Renders three kids of box: simple, box with fading effect and box with looping fade effect
 * 
 * @author m.sferra
 * 
 */
public class RenderingSystemTest extends BaseGameTest {

  public static void main(String[] args) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    new LwjglApplication(new RenderingSystemTest(), config);
  }

  private World physicWorld;
  private BoxTemplate boxTemplate;
  private FloorTemplate floorTemplate;
  private RunnerTemplate runnerTemplate;
  private AsteroidTemplate asteroidTemplate;

  @Override
  protected void createEntites() {
    physicWorld = new World(new Vector2(0, -100f), false);
    BodyBuilder bodyBuilder = new BodyBuilder(physicWorld);
    boxTemplate = new BoxTemplate(bodyBuilder);
    floorTemplate = new FloorTemplate(bodyBuilder);
    runnerTemplate = new RunnerTemplate(bodyBuilder);
    asteroidTemplate = new AsteroidTemplate(bodyBuilder);
    buildBoxes();
    buildRunner();
    buildFloor();
    buildAsteroid();
  }

  private void buildRunner() {
    entityFactory.instantiate(runnerTemplate,
        new Parameters().put("spatial", new SpatialImpl(400, 220, 100f, 90f)));
  }

  private void buildFloor() {
    entityFactory.instantiate(floorTemplate,
        new Parameters().put("spatial", new SpatialImpl(0, 10, camera.viewportWidth, 10, 0)));
  }

  private void buildAsteroid() {
    entityFactory.instantiate(asteroidTemplate,
        new Parameters().put("spatial", new SpatialImpl(500, 220, 40f, 40f)));
  }

  private void buildBoxes() {
    entityFactory.instantiate(boxTemplate,
        new Parameters().put("spatial", new SpatialImpl(100, 120, 20f, 20f)));
    entityFactory.instantiate(boxTemplate,
        new Parameters().put("spatial", new SpatialImpl(200, 220, 40f, 40f))).addComponent(
        new FadingComponent(3));
    entityFactory.instantiate(boxTemplate,
        new Parameters().put("spatial", new SpatialImpl(300, 220, 20f, 20f))).addComponent(
        new FadingComponent(3, true));
  }

  @Override
  protected void createLayers() {}

  @Override
  protected void addSystems() {
    world.setSystem(new PhysicsSystem(physicWorld));
    world.setSystem(new SpritePositionSystem());
    world.setSystem(new SpriteAnimationSystem());
    world.setSystem(new RenderableSystem(camera, renderableLayer));
    world.setSystem(new Box2dDebugSystem(camera, physicWorld));
    world.setSystem(new EntityDebugSystem());
    world.setSystem(new FpsDebugSystem());
  }

}
