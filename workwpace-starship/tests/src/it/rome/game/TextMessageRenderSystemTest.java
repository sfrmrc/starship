package it.rome.game;

import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.FadingComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.TextComponent;
import it.rome.game.system.FpsDebugSystem;
import it.rome.game.system.RenderableSystem;

import com.artemis.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Renders three kids of text entity
 * 
 * @author m.sferra
 * 
 */
public class TextMessageRenderSystemTest extends BaseGameTest {

  public static void main(String[] args) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    new LwjglApplication(new TextMessageRenderSystemTest(), config);
  }

  @Override
  protected void createEntites() {
    Texture fontTexture = new Texture(Gdx.files.internal("fonts/normal_0.png"));
    fontTexture.setFilter(TextureFilter.Linear, TextureFilter.MipMapLinearLinear);
    TextureRegion fontRegion = new TextureRegion(fontTexture);
    BitmapFont font = new BitmapFont(Gdx.files.internal("fonts/normal.fnt"), fontRegion, false);
    font.setUseIntegerPositions(false);

    RenderableComponent renderableComponent = new RenderableComponent(1);
    TextComponent redTextComponent = new TextComponent("red fixed message", font, Color.RED);
    TextComponent greenTextComponent = new TextComponent("green loop message", font, Color.GREEN);
    TextComponent yellowTextComponent =
        new TextComponent("yellow no loop message", font, Color.YELLOW);
    SpatialComponent redSpatialComponent = new SpatialComponent(new SpatialImpl(300, 200));
    SpatialComponent greenSpatialComponent = new SpatialComponent(new SpatialImpl(330, 250));
    SpatialComponent yellowSpatialComponent = new SpatialComponent(new SpatialImpl(250, 400));
    FadingComponent loopfadingComponent = new FadingComponent(3, true);
    FadingComponent noloopfadingComponent = new FadingComponent(3);

    Entity redText = world.createEntity();
    redText.addComponent(redTextComponent);
    redText.addComponent(redSpatialComponent);
    redText.addComponent(renderableComponent);
    world.addEntity(redText);

    Entity greenFeadingText = world.createEntity();
    greenFeadingText.addComponent(greenTextComponent);
    greenFeadingText.addComponent(greenSpatialComponent);
    greenFeadingText.addComponent(renderableComponent);
    greenFeadingText.addComponent(loopfadingComponent);
    world.addEntity(greenFeadingText);

    Entity yellowFeadingText = world.createEntity();
    yellowFeadingText.addComponent(yellowTextComponent);
    yellowFeadingText.addComponent(yellowSpatialComponent);
    yellowFeadingText.addComponent(renderableComponent);
    yellowFeadingText.addComponent(noloopfadingComponent);
    world.addEntity(yellowFeadingText);
  }

  @Override
  protected void createLayers() {}

  @Override
  protected void addSystems() {
    world.setSystem(new RenderableSystem(camera, renderableLayer));
    world.setSystem(new FpsDebugSystem());
  }


}
