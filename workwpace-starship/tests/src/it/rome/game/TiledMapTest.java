package it.rome.game;

import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.builder.EntityBuilder;
import it.rome.game.component.InputComponent;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.PovComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.system.Box2dDebugSystem;
import it.rome.game.system.CameraBoundsSystem;
import it.rome.game.system.CameraFocusSystem;
import it.rome.game.system.FpsDebugSystem;
import it.rome.game.system.MapRenderSystem;
import it.rome.game.system.PhysicsSystem;
import it.rome.game.system.WASDInputSystem;

import com.artemis.Entity;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

public class TiledMapTest extends BaseGameTest {

  public static void main(String[] args) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    new LwjglApplication(new TiledMapTest(), config);
  }

  private World physicWorld;
  private BodyBuilder bodyBuilder;

  @Override
  protected void createEntites() {
    physicWorld = new World(new Vector2(), true);
    bodyBuilder = new BodyBuilder(physicWorld);
    Body body =
        bodyBuilder
            .type(BodyType.DynamicBody)
            .position(320, 240)
            .fixture(
                bodyBuilder.fixtureDefBuilder().circleShape(20).restitution(0f).density(1f)
                    .friction(0f).build()).build();
    Entity player =
        new EntityBuilder(world).component(new InputComponent()).component(new MovementComponent())
            .component(new SpatialComponent(new SpatialPhysicsImpl(body, 40, 40)))
            .component(new PovComponent()).component(new VelocityComponent(200))
            .component(new PhysicsComponent(body)).build();
    world.addEntity(player);
  }

  @Override
  protected void createLayers() {}

  @Override
  protected void addSystems() {
    world.setSystem(new PhysicsSystem(physicWorld));
    world.setSystem(new MapRenderSystem(camera, "map/desert.tmx", entityFactory, bodyBuilder));
    world.setSystem(new CameraFocusSystem(camera));
    world.setSystem(new CameraBoundsSystem(camera, 1248, 1248));
    world.setSystem(new Box2dDebugSystem(camera, physicWorld));
    world.setSystem(new FpsDebugSystem());
    world.setSystem(new WASDInputSystem());
  }

}
