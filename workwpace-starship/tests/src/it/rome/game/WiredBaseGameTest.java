package it.rome.game;

import it.rome.game.builder.BodyBuilder;
import it.rome.game.builder.EntityBuilder;
import it.rome.game.component.InputComponent;
import it.rome.game.component.MovementComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.system.Box2dDebugSystem;
import it.rome.game.system.FpsDebugSystem;
import it.rome.game.system.PhysicsSystem;
import it.rome.game.system.WASDInputSystem;

import com.artemis.Entity;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Renders wired static box and one circle controlled by WASD input system
 * 
 * @author m.sferra
 * 
 */
public class WiredBaseGameTest extends BaseGameTest {

  public static void main(String[] args) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    new LwjglApplication(new WiredBaseGameTest(), config);
  }

  World physicWorld = new World(new Vector2(), true);
  Vector2 boxes[] = {new Vector2(50, 50), new Vector2(100, 100), new Vector2(250, 350),
      new Vector2(500, 400), new Vector2(300, 300)};
  BodyBuilder bodyBuilder = new BodyBuilder(physicWorld);

  @Override
  protected void createEntites() {
    EntityBuilder entityBuilder = new EntityBuilder(world);
    for (int i = 0; i < 5; i++) {
      Body body =
          bodyBuilder
              .type(BodyType.StaticBody)
              .position(boxes[i].x, boxes[i].y)
              .fixture(
                  bodyBuilder.fixtureDefBuilder().boxShape(20, 20).restitution(0f).density(1f)
                      .friction(1f).build()).build();
      entityBuilder.component(new PhysicsComponent(body)).build();
    }
    Body body =
        bodyBuilder
            .type(BodyType.DynamicBody)
            .position(320, 240)
            .fixture(
                bodyBuilder.fixtureDefBuilder().circleShape(20).restitution(0f).density(1f)
                    .friction(0f).build()).build();
    Entity entity =
        entityBuilder.component(new InputComponent()).component(new MovementComponent())
            .component(new VelocityComponent(200)).component(new PhysicsComponent(body)).build();
    world.addEntity(entity);
  }

  @Override
  protected void createLayers() {}

  @Override
  protected void addSystems() {
    world.setSystem(new Box2dDebugSystem(camera, physicWorld));
    world.setSystem(new FpsDebugSystem());
    world.setSystem(new FpsDebugSystem());
    world.setSystem(new WASDInputSystem());
    world.setSystem(new PhysicsSystem(physicWorld));
  }

}
