package it.rome.game.builder;

import static org.mockito.Mockito.mock;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class MockBodyBuilder extends BodyBuilder {

  public MockBodyBuilder() {
    super(null);
  }

  public FixtureDefBuilder fixtureDefBuilder() {
    return new MockFixtureDefBuilder();
  }

  public BodyBuilder type(BodyType bodyType) {
    return this;
  }

  public BodyBuilder fixture(FixtureDefBuilder fixtureDef) {
    return this;
  }

  public BodyBuilder fixture(FixtureDefBuilder fixtureDef, Object fixtureUserData) {
    return this;
  }

  public BodyBuilder fixture(FixtureDef fixtureDef) {
    return this;
  }

  public BodyBuilder fixture(FixtureDef fixtureDef, Object fixtureUserData) {
    return this;
  }

  public BodyBuilder mass(float mass) {
    return this;
  }

  public BodyBuilder userData(Object userData) {
    return this;
  }

  public BodyBuilder position(float x, float y) {
    return this;
  }

  @Override
  public Body build() {
    return mock(Body.class);
  }

}
