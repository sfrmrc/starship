package it.rome.game.builder;

import static org.mockito.Mockito.mock;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class MockFixtureDefBuilder extends FixtureDefBuilder {

  public MockFixtureDefBuilder() {}

  public FixtureDefBuilder sensor() {
    return this;
  }

  public FixtureDefBuilder boxShape(float hx, float hy) {
    return this;
  }

  public FixtureDefBuilder boxShape(float hx, float hy, Vector2 center, float angleInRadians) {
    return this;
  }

  public FixtureDefBuilder circleShape(float radius) {
    return this;
  }

  public FixtureDefBuilder circleShape(Vector2 center, float radius) {
    return this;
  }

  public FixtureDefBuilder polygonShape(Vector2[] vertices) {
    return this;
  }

  public FixtureDefBuilder density(float density) {
    return this;
  }

  public FixtureDefBuilder friction(float friction) {
    return this;
  }

  public FixtureDefBuilder restitution(float restitution) {
    return this;
  }

  public FixtureDefBuilder categoryBits(short categoryBits) {
    return this;
  }

  public FixtureDefBuilder maskBits(short maskBits) {
    return this;
  }

  public FixtureDef build() {
    return mock(FixtureDef.class);
  }

}
