package it.rome.game.fsm;

import org.junit.Assert;
import org.junit.Test;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.fsm.EntityStateMachine;

public class EntityStateMachineTest {

  @Test
  public void testCreate() {
    World world = new World();
    world.initialize();
    Entity entity = world.createEntity();
    EntityStateMachine<State> esm = new EntityStateMachine<State>(entity);
    esm.createState(State.BEGIN).add(BeginComponent.class);
    esm.createState(State.WORK).add(WorkComponent.class);
    esm.createState(State.END).add(EndComponent.class);
    Assert.assertTrue(entity.getComponent(BeginComponent.class) == null);
    Assert.assertTrue(entity.getComponent(WorkComponent.class) == null);
    Assert.assertTrue(entity.getComponent(EndComponent.class) == null);
    esm.changeState(State.BEGIN);
    Assert.assertTrue(entity.getComponent(BeginComponent.class) != null);
    Assert.assertTrue(entity.getComponent(WorkComponent.class) == null);
    Assert.assertTrue(entity.getComponent(EndComponent.class) == null);
    esm.changeState(State.WORK);
    Assert.assertTrue(entity.getComponent(BeginComponent.class) == null);
    Assert.assertTrue(entity.getComponent(WorkComponent.class) != null);
    Assert.assertTrue(entity.getComponent(EndComponent.class) == null);
    esm.changeState(State.END);
    Assert.assertTrue(entity.getComponent(BeginComponent.class) == null);
    Assert.assertTrue(entity.getComponent(WorkComponent.class) == null);
    Assert.assertTrue(entity.getComponent(EndComponent.class) != null);
  }

  @Test
  public void testCreate2() {
    World world = new World();
    world.initialize();
    Entity entity = world.createEntity();
    EntityStateMachine<State> esm = new EntityStateMachine<State>(entity);
    BeginComponent c1 = new BeginComponent();
    c1.string = "c1";
    BeginComponent c2 = new BeginComponent();
    c2.string = "c2";
    esm.createState(State.BEGIN).add(BeginComponent.class).withInstance(c1);
    esm.createState(State.END).add(BeginComponent.class).withInstance(c2);
    esm.changeState(State.BEGIN);
    Assert.assertEquals("c1", entity.getComponent(BeginComponent.class).string);
    esm.changeState(State.END);
    Assert.assertEquals("c2", entity.getComponent(BeginComponent.class).string);
  }

  private enum State {
    BEGIN, WORK, END
  }

}
