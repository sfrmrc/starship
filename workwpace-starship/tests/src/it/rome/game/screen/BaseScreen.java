package it.rome.game.screen;

import it.rome.game.system.render.Layer;
import it.rome.game.system.render.RenderableLayer;
import it.rome.game.template.EntityFactory;

import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class BaseScreen extends ScreenAdapter {

  protected World world;
  protected EntityFactory entityFactory;
  protected SpriteBatch spriteBatch;
  protected RenderableLayer renderableLayer;
  protected OrthographicCamera camera;

  public BaseScreen() {
    float w = Gdx.graphics.getWidth();
    float h = Gdx.graphics.getHeight();
    camera = new OrthographicCamera(w, h);
    camera.setToOrtho(false, w, h);
    camera.update();
    world = new World();
    world.setManager(new TagManager());
    world.setManager(new GroupManager());
    entityFactory = new EntityFactory(world);
    world.initialize();

    createEntites();

    Layer layer = new Layer(1, camera, new SpriteBatch());
    renderableLayer = new RenderableLayer();
    renderableLayer.add("ui", layer);

    addSystems();

  }

  protected abstract void createEntites();

  protected abstract void createLayers();

  protected abstract void addSystems();

  @Override
  public void render(float delta) {
    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    world.setDelta(delta);
    world.process();
  }

}
