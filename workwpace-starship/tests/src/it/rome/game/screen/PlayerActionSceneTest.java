package it.rome.game.screen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public class PlayerActionSceneTest extends Game {

  public static void main(String[] args) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    new LwjglApplication(new PlayerActionSceneTest(), config);
  }

  Screen screen;

  @Override
  public void create() {
    screen = new MainScreen();
    setScreen(screen);
  }

  class MainScreen extends BaseScreen {

    private Stage stage;

    @Override
    protected void createEntites() {
      final Label label = new Label("prova", new LabelStyle(new BitmapFont(), Color.WHITE));
      label.setTouchable(Touchable.enabled);
      label.setBounds(0, 0, label.getWidth(), label.getHeight());
      label.addListener(new InputListener() {
        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
          label.setVisible(false);
          return true;
        }
      });
      stage = new Stage();
      Gdx.input.setInputProcessor(stage);
      stage.addActor(label);
    }

    @Override
    protected void createLayers() {}

    @Override
    protected void addSystems() {}

    @Override
    public void render(float delta) {
      super.render(delta);
      stage.act(delta);
      stage.draw();
    }

  }

}
