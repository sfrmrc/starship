package it.rome.game.system;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import it.rome.game.BaseWorldTest;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.DamageComponent;
import it.rome.game.component.HealtComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.template.EntityFactory;
import it.rome.game.template.EntityTemplate;
import it.rome.game.template.Parameters;

import org.junit.Test;

import com.artemis.EntityEdit;

public class DamageSystemTest extends BaseWorldTest {

  EntityFactory entityFactory;
  EntityTemplate entityTemplate;
  DamageComponent damageComponent;
  HealtComponent healtComponent;
  SpatialComponent spatialComponent;
  DamageSystem damageSystem;

  @Override
  protected void initSystem() {
    entityFactory = mock(EntityFactory.class);
    entityTemplate = mock(EntityTemplate.class);
    damageSystem = new DamageSystem(entityFactory, entityTemplate);
    world.setSystem(damageSystem);
  }

  @Override
  protected void initEntities() {
    damageComponent = new DamageComponent(10);
    healtComponent = new HealtComponent(100);
    spatialComponent = new SpatialComponent(new SpatialImpl(10, 10));
  }

  @Test
  public void integrationTest() {
    EntityEdit entity = world.createEntity().edit();
    entity.add(damageComponent);
    entity.add(healtComponent);
    int postHealt = healtComponent.getLife() - damageComponent.getDamage();
    damageSystem.process(entity.getEntity());
    verify(entityFactory, times(0)).instantiate(entityTemplate);
    assertEquals(postHealt, healtComponent.getLife());
    entity.add(new SpatialComponent(new SpatialImpl(10, 10)));
    EntityEdit entity1 = world.createEntity().edit();
    entity1.add(damageComponent);
    entity1.add(healtComponent);
    entity1.add(spatialComponent);
    postHealt = healtComponent.getLife() - damageComponent.getDamage();
    damageSystem.process(entity1.getEntity());
    verify(entityFactory, times(1)).instantiate(any(EntityTemplate.class), any(Parameters.class));
    assertEquals(postHealt, healtComponent.getLife());
  }
}
