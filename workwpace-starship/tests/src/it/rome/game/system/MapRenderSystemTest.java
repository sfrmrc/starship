package it.rome.game.system;

import it.rome.game.BaseGameTest;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class MapRenderSystemTest extends BaseGameTest {

  public static void main(String[] args) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    new LwjglApplication(new MapRenderSystemTest(), config);
  }

  @Override
  protected void createEntites() {}

  @Override
  protected void createLayers() {}

  @Override
  protected void addSystems() {
    MapRenderSystem mapSystem = new MapRenderSystem(camera, "desert.tmx");
    world.setSystem(mapSystem);
  }

}
