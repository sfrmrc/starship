package it.rome.game.system.render;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import it.rome.game.BaseWorldTest;
import it.rome.game.attribute.Spatial;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteComponent;

import org.junit.Test;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * 
 * @author m.sferra
 * 
 */
public class LayerTest extends BaseWorldTest {

  OrthographicCamera camera = mock(OrthographicCamera.class);
  SpriteBatch spriteBatch = mock(SpriteBatch.class);
  EntityEdit entityEdit;
  SpriteComponent spriteComponent;
  SpatialComponent spatialComponent;
  RenderableComponent renderableComponent;
  Sprite sprite;

  @Override
  protected void initSystem() {}

  @Override
  protected void initEntities() {
    entityEdit = world.createEntity().edit();
    Spatial spatial = mock(Spatial.class);
    spatialComponent = new SpatialComponent(spatial);
    sprite = mock(Sprite.class);
    spriteComponent = new SpriteComponent(sprite, Color.BLACK);
    renderableComponent = new RenderableComponent(1, true);
    entityEdit.add(renderableComponent);
    entityEdit.add(spriteComponent);
    entityEdit.add(spatialComponent);
  }

  @Test
  public void createAddEntity() {
    Entity entity = entityEdit.getEntity();
    Layer layer = new Layer(1, camera, spriteBatch);
    assertEquals(layer.getLevel(), 1);
    assertNotNull(layer);
    layer.add(entity);
    assertTrue(layer.contains(entity));
    assertTrue(layer.isEnabled());
    layer.render();
    verify(sprite, times(1)).draw(spriteBatch);
    layer.setEnabled(false);
    assertFalse(layer.isEnabled());
    layer.remove(entity);
  }

}
