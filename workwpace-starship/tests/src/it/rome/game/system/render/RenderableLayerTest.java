package it.rome.game.system.render;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author m.sferra
 *
 */
public class RenderableLayerTest {

  @Test
  public void create() {
    Layer l1 = mock(Layer.class);
    RenderableLayer renderableLayer = new RenderableLayer();
    renderableLayer.add("l1", l1);
    assertEquals(1, renderableLayer.layers().size());
    assertSame(l1, renderableLayer.layer("l1"));
    Layer l2 = mock(Layer.class);
    renderableLayer.add("l2", l2);
    assertEquals(2, renderableLayer.layers().size());
    assertSame(l2, renderableLayer.layer("l2"));
    renderableLayer.remove("l1");
    assertEquals(1, renderableLayer.layers().size());
    assertNull(renderableLayer.get("l1"));
    renderableLayer.disable("l2");
    verify(l2, times(1)).setEnabled(false);
  }
  
}
