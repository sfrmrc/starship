package it.rome.game.template;

import static org.mockito.Mockito.mock;
import it.rome.game.BaseWorldTest;
import it.rome.game.component.DamageComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.star.game.template.DamageTextTemplate;

import org.junit.Test;

import com.artemis.Entity;
import com.badlogic.gdx.graphics.g2d.BitmapFont;


public class DamageTextTemplateTest extends BaseWorldTest {

  Entity entity;

  @Override
  protected void initSystem() {}

  @Override
  protected void initEntities() {
    entity = world.createEntity();
  }

  @Test(expected = IllegalArgumentException.class)
  public void createFailSpatial() {
    DamageTextTemplate entityTemplate = new DamageTextTemplate();
    entityTemplate.setParameters(new Parameters());
    entityTemplate.apply(entity);
  }

  @Test(expected = IllegalArgumentException.class)
  public void createFailDamage() {
    DamageTextTemplate entityTemplate = new DamageTextTemplate();
    entityTemplate.setParameters(new Parameters().put("spatial", mock(SpatialComponent.class)));
    entityTemplate.apply(entity);
  }

  @Test
  public void create() {
    DamageTextTemplate entityTemplate = new DamageTextTemplate();
    Parameters parameters =
        new Parameters().put("spatial", mock(SpatialComponent.class))
            .put("damage", mock(DamageComponent.class)).put("font", mock(BitmapFont.class));
    entityTemplate.setParameters(parameters);
    entityTemplate.apply(entity);
  }

}
