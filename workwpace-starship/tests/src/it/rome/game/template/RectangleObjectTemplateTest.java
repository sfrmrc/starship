package it.rome.game.template;

import static org.junit.Assert.assertNotNull;
import it.rome.game.BaseWorldTest;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.builder.MockBodyBuilder;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.template.map.RectangleObjectTemplate;

import org.junit.Test;
import org.mockito.Mockito;

import com.artemis.Entity;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;

public class RectangleObjectTemplateTest extends BaseWorldTest {

  BodyBuilder bodyBuilder;

  @Override
  protected void initSystem() {}

  @Override
  protected void initEntities() {
    bodyBuilder = new MockBodyBuilder();
  }

  @Test(expected = IllegalArgumentException.class)
  public void createFail() {
    Entity entity = null;
    try {
      RectangleObjectTemplate obstacleTemplate = new RectangleObjectTemplate(bodyBuilder);
      entity = world.createEntity();
      obstacleTemplate.apply(entity);
    } finally {
      if (entity != null) entity.deleteFromWorld();
    }
  }

  @Test
  public void create() {
    Entity entity = null;
    try {
      RectangleMapObject mapObject = Mockito.mock(RectangleMapObject.class);
      Mockito.when(mapObject.getRectangle()).thenReturn(Mockito.mock(Rectangle.class));
      Parameters parameters =
          new Parameters().put("x", 1f).put("y", 2f).put("h", 3f).put("w", 3f)
              .put("object", mapObject);
      RectangleObjectTemplate obstacleTemplate = new RectangleObjectTemplate(bodyBuilder);
      entity = world.createEntity();
      obstacleTemplate.setParameters(parameters);
      obstacleTemplate.apply(entity);
      assertNotNull(entity.getComponent(SpatialComponent.class));
      assertNotNull(entity.getComponent(PhysicsComponent.class));
    } finally {
      if (entity != null) entity.deleteFromWorld();
    }
  }

}
