package it.rome.game.template;

import static org.junit.Assert.assertNotNull;
import it.rome.game.BaseWorldTest;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.star.game.template.StarshipTemplate;

import org.junit.Test;

import com.artemis.Entity;

public class StarshipTemplateTest extends BaseWorldTest {

  private EntityFactory factory;
  private BodyBuilder builder;

  @Override
  protected void initSystem() {}

  @Override
  protected void initEntities() {
    factory = new EntityFactory(world);
    builder = new BodyBuilder(physicWorld);
  }

  @Test
  public void createEntitySuccess() {
    Entity entity =
        factory.instantiate(new StarshipTemplate(builder),
            new Parameters().put("spatial", new SpatialImpl(0, 0, 10, 10)));
    assertNotNull(entity);
  }

  @Test(expected = IllegalArgumentException.class)
  public void createEntityFail() {
    Entity entity = factory.instantiate(new StarshipTemplate(builder));
    assertNotNull(entity);
  }

}
