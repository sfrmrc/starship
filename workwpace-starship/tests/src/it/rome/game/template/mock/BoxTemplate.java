package it.rome.game.template.mock;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.PhysicsImpl;
import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * 
 * @author m.sferra
 * 
 */
public class BoxTemplate extends EntityTemplate {

  private BodyBuilder bodyBuilder;

  public BoxTemplate(BodyBuilder bodyBuilder) {
    this.bodyBuilder = bodyBuilder;
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spatial spatial = parameters.get("spatial");

    if (spatial == null) throw new IllegalArgumentException("Spatial parameter cannot be null");

    Body body =
        bodyBuilder
            .userData(entity)
            .type(BodyType.DynamicBody)
            .position(spatial.getX(), spatial.getY())
            .fixture(
                bodyBuilder.fixtureDefBuilder()
                    .boxShape(spatial.getWidth() * 0.5f, spatial.getHeight() * 0.5f)
                    .restitution(0.3f).density(1f).friction(0.5f).build()).build();

    Texture texture = new Texture("crate.png");
    Sprite sprite = new Sprite(texture);

    entityEdit.add(new TagComponent("Box"));
    entityEdit.add(new RenderableComponent(1));
    entityEdit.add(new PhysicsComponent(new PhysicsImpl(body)));
    entityEdit.add(new SpriteComponent(sprite));
    entityEdit.add(new SpatialComponent(new SpatialPhysicsImpl(body, spatial)));
  }
}
