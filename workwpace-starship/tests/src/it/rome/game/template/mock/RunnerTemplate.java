package it.rome.game.template.mock;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteAnimationComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * 
 * @author m.sferra
 * 
 */
public class RunnerTemplate extends EntityTemplate {

  private BodyBuilder bodyBuilder;

  public RunnerTemplate(BodyBuilder bodyBuilder) {
    this.bodyBuilder = bodyBuilder;
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spatial spatial = parameters.get("spatial");

    Texture texture = new Texture(Gdx.files.internal("runner.png"));
    SpriteAnimationComponent spriteAnimationComponent =
        new SpriteAnimationComponent(texture, 0f, 0.025f, PlayMode.LOOP, 6, 5);

    Body body =
        bodyBuilder
            .userData(entity)
            .type(BodyType.DynamicBody)
            .position(spatial.getX(), spatial.getY())
            .fixture(
                bodyBuilder.fixtureDefBuilder()
                    .boxShape(spatial.getWidth() * 0.5f, spatial.getHeight() * 0.5f)
                    .restitution(0.3f).density(1f).friction(0.5f).build()).build();

    entityEdit.add(new PhysicsComponent(body));
    entityEdit.add(new RenderableComponent(1));
    entityEdit.add(new SpriteComponent(new Sprite(texture)));
    entityEdit.add(spriteAnimationComponent);
    entityEdit.add(new SpatialComponent(new SpatialPhysicsImpl(body, spatial)));
  }

}
